package denik.tools.avro.sql.job

import java.io.File

import com.databricks.spark.avro._
import com.typesafe.config.Config
import org.apache.avro.Schema
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.JavaConversions._

/**
  * Created by denik on 23.11.2017.
  */
object JobRunner {

  val logger: Logger = LoggerFactory.getLogger(JobRunner.getClass)

  def main(args: Array[String]): Unit = {

    val configuration = Configuration.buildFromFile(args(0))

    val sparkConf = new SparkConf()
    copyParamsToSparkConf(sparkConf, configuration.appConfig.getConfig("spark").atPath("spark"))

    val sparkSession = SparkSession.builder().config(sparkConf).getOrCreate()

    //define all tables
    configuration.pipeline.tables.foreach {
      td =>


        //        val schema = new Schema.Parser().parse(
        //          new File("/projects/pegasus/codebase/avro-random-generator/src/test/resources/avro-1.8.1/sample.avsc")
        //        )


        val dfs = td._2.avros.map {
          avro =>
            sparkSession
              .read
              .format("com.databricks.spark.avro")
              //    .option("avroSchema", schema.toString)
              .load(avro)
        }
        val unionDf: Option[DataFrame] = if (dfs.length > 1) {
          Some(dfs.tail.foldLeft(dfs(0)) {
            (u, d) =>
              u.union(d)
          })
        } else if (dfs.length == 1) {
          Some(dfs(0))
        } else {
          None
        }

        unionDf.foreach {
          udf =>
            udf.createOrReplaceTempView(td._1)
        }
    }

    println(sparkSession.sqlContext.tableNames().toList)

    //execute sql with tables
    configuration.pipeline.exe.foreach {
      sql =>
        val rdf = sparkSession.sql(sql._2.sql)
        sql._2.out.map {
          outValue =>
            rdf.write.avro(outValue)
        }.getOrElse {
          rdf.show()
        }
    }

  }

  def copyParamsToSparkConf(sparkConf: SparkConf, config: Config): Unit = {
    config.entrySet().foreach {
      v =>
        sparkConf.set(v.getKey, "" + v.getValue.unwrapped())
    }
  }


}
