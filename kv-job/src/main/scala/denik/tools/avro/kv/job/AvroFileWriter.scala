package denik.tools.avro.kv.job

import java.io.{BufferedOutputStream, File, FileOutputStream}

import org.apache.avro.Schema
import org.apache.avro.file.DataFileWriter
import org.apache.avro.generic.{GenericDatumWriter, GenericRecord}
import org.slf4j.LoggerFactory

/**
  * Created by denik on 13.12.2017.
  */
class AvroFileWriter(s: Schema, dir: String, prefix: String, numberOfDocs: Int = 100000) {

  private val logger = LoggerFactory.getLogger(classOf[AvroFileWriter])

  var fileIndex = 0
  var recordIndex = 0
  var output: Option[DataFileWriter[GenericRecord]] = None

  open()

  def writeDoc(avro: GenericRecord): Unit = {
    output.foreach {
      os =>
        os.append(avro)
        recordIndex += 1
        if (recordIndex == numberOfDocs) {
          close()
          open()
        }
    }
  }

  def open(): Unit = {
    fileIndex += 1
    recordIndex = 0

    new File(s"$dir").mkdirs()

    output = Some {
      val w = new DataFileWriter[GenericRecord](new GenericDatumWriter[GenericRecord]())
      w.create(s, new BufferedOutputStream(new FileOutputStream(s"$dir/$prefix.$fileIndex.avro")))
    }
  }


  def close(): Unit = {
    output.foreach {
      os =>
        try {
          os.flush()
        } catch {
          case t: Throwable => logger.error("error while flushing stream", t)
        }
        try {
          os.close()
        } catch {
          case t: Throwable => logger.error("error while closing stream", t)
        }
    }
    output = None
  }

}
