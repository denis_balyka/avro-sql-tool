package denik.tools.avro.kv.job

import java.io.File

import org.apache.commons.io.FileUtils
import org.rocksdb.RocksDB
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Try}

/**
  * Created by denik on 12.12.2017.
  */
object RocksdbOperations {

  val logger: Logger = LoggerFactory.getLogger(JobRunner.getClass)

  def openDb(configuration: Configuration): Try[RocksDB] = {
    val result = Try {
      RocksDB.loadLibrary()
      val options = configuration.rocksdbOptions.asOptions()
      val db = RocksDB.open(options, configuration.rocksdbOptions.dbPath)
      db
    }
    result match {
      case f: Failure[_] => logger.error("error while db opening", f.exception)
      case _ =>
    }
    result
  }

  def openDbs(configuration: Configuration, exe: String): Try[Dbs] = {

    val result = Try {
      val exeCfg = configuration.pipeline.exe.get(exe).get
      RocksDB.loadLibrary()
      val options = configuration.rocksdbOptions.asOptions()

      val dbJoinDir = s"${configuration.rocksdbOptions.dbPath}/${exeCfg.joinTable}"
      val dbJoinFieldDir = s"${configuration.rocksdbOptions.dbPath}/${exeCfg.joinTable}-${exeCfg.joinTableField.mkString(".")}"

      FileUtils.forceMkdir(new File(dbJoinDir))
      FileUtils.forceMkdir(new File(dbJoinFieldDir))

      val dbJoin = RocksDB.open(options, dbJoinDir)
      val dbJoinField = RocksDB.open(options, dbJoinFieldDir)
      Dbs(dbJoin, dbJoinField)
    }
    result match {
      case f: Failure[_] => logger.error("error while db opening", f.exception)
      case _ =>
    }
    result
  }


}

case class Dbs(joinTableDb: RocksDB, joinTableFieldDb: RocksDB)