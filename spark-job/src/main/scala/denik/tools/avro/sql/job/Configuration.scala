package denik.tools.avro.sql.job

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._

import scala.collection.JavaConversions._
import scala.util.Try

/**
  * Created by denik on 23.11.2017.
  */
object Configuration {
  def buildFromFile(fileName: String): Configuration = {
    apply(ConfigFactory.load(ConfigFactory.parseFile(new File(fileName))))
  }

  def buildPipeline(appConfig: Config): Pipeline = {
    appConfig.as[Pipeline]("pipeline")
  }

  def apply(appConfig: Config): Configuration = {
    val pipeline = buildPipeline(appConfig)

    Configuration(
      appConfig,
      pipeline
    )

  }


}

case class Configuration(appConfig: Config, pipeline: Pipeline)

case class Pipeline(tables: Map[String, TableData], exe: Map[String, SqlExecutionData])

case class TableData(avros: Array[String], schema: Option[String])

case class SqlExecutionData(sql: String, out: Option[String])