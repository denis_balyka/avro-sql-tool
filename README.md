# avro-sql-tool

## build
```
mvn clean package
```
during build uberjars are created

## sample data

person avro schema
```
{
  "namespace": "sample.avro.model",
  "name": "Person",
  "type": "record",
  "fields": [
    {
      "name": "id",
      "type": "int"
    },
    {
      "name": "name",
      "type": "string"
    },
    {
      "name": "code",
      "type": {
        "type": "fixed",
        "size": 3,
        "name": "code3"
      }
    }
  ]
}
```

order avro schema
```
{
  "namespace": "sample.avro.model",
  "name": "Order",
  "type": "record",
  "fields": [
    {
      "name": "id",
      "type": "int"
    },
    {
      "name": "description",
      "type": "string"
    },
    {
      "name": "code",
      "type": {
        "type": "fixed",
        "size": 3,
        "name": "code3"
      }
    }
  ]
}
```

joined schema
```
{
  "type": "record",
  "name": "topLevelRecord",
  "fields": [
    {
      "name": "pid",
      "type": [
        "int",
        "null"
      ]
    },
    {
      "name": "pname",
      "type": [
        "string",
        "null"
      ]
    },
    {
      "name": "code",
      "type": [
        "bytes",
        "null"
      ]
    },
    {
      "name": "oid",
      "type": [
        "int",
        "null"
      ]
    },
    {
      "name": "odescription",
      "type": [
        "string",
        "null"
      ]
    }
  ]
}
```

### data generation
using https://bitbucket.org/denis_balyka/avro-random-generator generate persons and orders

```
java -jar ./target/avro-random-generator-0.0.5-SNAPSHOT-1.8.1.jar -s ./src/test/resources/avro-1.8.1/person.avsc -c bzip2 -t 16 -f 16 -n false -r 1000 -o /tmp/avro-person
java -jar ./target/avro-random-generator-0.0.5-SNAPSHOT-1.8.1.jar -s ./src/test/resources/avro-1.8.1/order.avsc -c bzip2 -t 16 -f 128 -n false -r 1000 -o /tmp/avro-order
```



## spark sql based solution
### run uberjar 
```
java -cp spark-job/target/spark-job-0.0.1-SNAPSHOT-all.jar denik.tools.avro.sql.job.JobRunner spark-job/src/test/resources/avro-sql-tool.conf
```

### run sql on avro tables

avro-sql-tool config for local execution
```
{
  pipeline {
    tables {
      orders: {
        avros: [
          "/tmp/avro-order"
        ]
      }
      persons: {
        avros: [
          "/tmp/avro-person"
        ]
      }
    }
    exe: {
      join: {
        sql: """
          select
            p.id as pid,
            p.name as pname,
            p.code as code,
            o.id as oid,
            o.description as odescription
          from persons p
          join orders o on p.code = o.code
        """
        out: "file:///tmp/join-out"
      }
    }
  }

  spark {
    app.name: avro-sql-tool
    sql.avro.compression.codec: bzip2
    master: "local[*]"
    io.compression.codec: lz4
  }
}
```

### performance metrics

| table | records count | size on disk | compression |
| :------: | -------: | -----: | :------: |
|persons | `16000000 (16M) | 0.264Gb | bzip2|
|orders | `128000000 (128M) | 2Gb | bzip2|
|join result | `2388793932 (2.4B) |100Gb |null|

processing time 24 minutes on laptop with core i7, 32gb ram & 1Tb ssd


## rocksdb based solution
### run uberjar 
```
java -cp kv-job/target/kv-job-0.0.1-SNAPSHOT-all.jar denik.tools.avro.kv.job.JobRunner kv-job/src/test/resources/avro-kv-tool.conf
```

### run join on avro data

avro-kv-tool config for local execution
```
{
  pipeline {
    tables {
      orders: {
        avros: [
          "/tmp/avro-order"
        ]
      }
      persons: {
        avros: [
          "/tmp/avro-person"
        ]
      }
    }
    exe: {
      join1: {
        table: orders
        joinTable: persons
        tableField: "code"
        joinTableField: ["code"]
        out: "C:/tmp/kv-join-out"
      }
    }
  }

    rocksdb {
      path: "/tmp/rdbs"
      db {
        stats_dump_period_sec: 600
        max_manifest_file_size: 18446744073709551615
        bytes_per_sync: 83886080
        delayed_write_rate: 2097152
        WAL_ttl_seconds: 0
        WAL_size_limit_MB: 0
        max_subcompactions: 1
        wal_dir: ""
        wal_bytes_per_sync: 0
        db_write_buffer_size: 0
        keep_log_file_num: 1000
        table_cache_numshardbits: 4
        max_file_opening_threads: 1
        writable_file_max_buffer_size: 10485760
        random_access_max_buffer_size: 10485760
        use_fsync: false
        max_total_wal_size: 0
        max_open_files: -1
        skip_stats_update_on_db_open: false
        max_background_compactions: 16
        manifest_preallocation_size: 4194304
        max_background_flushes: 7
        is_fd_close_on_exec: true
        max_log_file_size: 0
        advise_random_on_open: true
        create_missing_column_families: true
        paranoid_checks: true
        delete_obsolete_files_period_micros: 21600000000
        log_file_time_to_roll: 0
        compaction_readahead_size: 0
        create_if_missing: true
        use_adaptive_mutex: false
        enable_thread_tracking: false
        allow_fallocate: true
        error_if_exists: false
        recycle_log_file_num: 0
        skip_log_error_on_recovery: false
        db_log_dir: ""
        new_table_reader_for_compaction_inputs: true
        allow_mmap_reads: false
        allow_mmap_writes: false
        use_direct_reads: false
        use_direct_writes: false
    
      }
      cf {
        compaction_style: kCompactionStyleLevel
        compaction_filter: nullptr
        num_levels: 6
        table_factory: BlockBasedTable
        comparator: leveldb.BytewiseComparator
        max_sequential_skip_in_iterations: 8
        soft_rate_limit: 0.000000
        max_bytes_for_level_base: 1073741824
        memtable_prefix_bloom_probes: 6
        memtable_prefix_bloom_bits: 0
        memtable_prefix_bloom_huge_page_tlb_size: 0
        max_successive_merges: 0
        arena_block_size: 16777216
        min_write_buffer_number_to_merge: 1
        target_file_size_multiplier: 1
        source_compaction_factor: 1
        max_bytes_for_level_multiplier: 8
        max_bytes_for_level_multiplier_additional: "2:3:5"
        compaction_filter_factory: nullptr
        max_write_buffer_number: 8
        level0_stop_writes_trigger: 20
        compression: kNoCompression //kSnappyCompression
        level0_file_num_compaction_trigger: 4
        purge_redundant_kvs_while_flush: true
        max_write_buffer_number_to_maintain: 0
        memtable_factory: SkipListFactory
        max_grandparent_overlap_factor: 8
        expanded_compaction_factor: 25
        hard_pending_compaction_bytes_limit: 137438953472
        inplace_update_num_locks: 10000
        level_compaction_dynamic_level_bytes: true
        level0_slowdown_writes_trigger: 12
        filter_deletes: false
        verify_checksums_in_compaction: true
        min_partial_merge_operands: 2
        paranoid_file_checks: false
        target_file_size_base: 134217728
        optimize_filters_for_hits: false
        merge_operator: PutOperator
        compression_per_level: "kNoCompression:kNoCompression:kNoCompression:kNoCompression:kNoCompression:kNoCompression"
        //"kNoCompression:kNoCompression:kNoCompression:kSnappyCompression:kSnappyCompression:kSnappyCompression"
        compaction_measure_io_stats: false
        prefix_extractor: nullptr
        bloom_locality: 0
        write_buffer_size: 134217728
        disable_auto_compactions: false
        inplace_update_support: false
      }
    }
  
}
```
