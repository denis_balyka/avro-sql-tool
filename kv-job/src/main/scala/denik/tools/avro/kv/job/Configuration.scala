package denik.tools.avro.kv.job

import java.io.File
import java.util.Properties

import com.typesafe.config.{Config, ConfigFactory}
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import org.rocksdb
import org.rocksdb.{ColumnFamilyOptions, DBOptions, Options}

import scala.collection.JavaConversions._

/**
  * Created by denik on 23.11.2017.
  */
object Configuration {
  def buildFromFile(fileName: String): Configuration = {
    apply(ConfigFactory.load(ConfigFactory.parseFile(new File(fileName))))
  }

  def buildPipeline(appConfig: Config): Pipeline = {
    appConfig.as[Pipeline]("pipeline")
  }

  def apply(appConfig: Config): Configuration = {
    val pipeline = buildPipeline(appConfig)


    Configuration(
      appConfig,
      pipeline,
      RocksdbOptions(
        appConfig.getString("rocksdb.path"),
        DBOptions.getDBOptionsFromProps(getConfigAsProperties(appConfig, "rocksdb.db")),
        ColumnFamilyOptions.getColumnFamilyOptionsFromProps(getConfigAsProperties(appConfig, "rocksdb.cf"))
      )
    )

  }

  def getConfigAsProperties(appConfig: Config, path: String): Properties = {
    val p = new Properties()
    appConfig.getConfig(path).entrySet().foreach {
      e =>
        p.setProperty(e.getKey, "" + e.getValue.unwrapped())
    }
    p
  }

}

case class Configuration(appConfig: Config,
                         pipeline: Pipeline,
                         rocksdbOptions: RocksdbOptions)

case class Pipeline(tables: Map[String, TableData],
                    exe: Map[String, SqlExecutionData])

case class TableData(avros: Array[String],
                     schema: Option[String])

case class SqlExecutionData(table: String,
                            joinTable: String,
                            tableField: String,
                            joinTableField: Array[String],
                            out: String)

case class RocksdbOptions(dbPath: String, db: DBOptions, cf: ColumnFamilyOptions) {
  def asOptions(): Options = {
    new Options(db, cf)
  }
}