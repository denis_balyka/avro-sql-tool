package denik.tools.avro.kv.job

import java.util
import java.util.concurrent.atomic.AtomicLong

import com.google.common.primitives.Longs
import org.apache.commons.codec.digest.DigestUtils
import org.rocksdb.ReadOptions
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Try}

/**
  * Created by denik on 12.12.2017.
  */
object JobRunner {
  val logger: Logger = LoggerFactory.getLogger(JobRunner.getClass)

  def main(args: Array[String]): Unit = {

    val configuration = Configuration.buildFromFile(args(0))
    val schemas = AvroTableOperations.tableDataSchemas(configuration)

    configuration.pipeline.exe.foreach {
      exe =>
        val joinTableId = Array.ofDim[Byte](24)
        RocksdbOperations.openDbs(configuration, exe._1).toOption.foreach {
          rdbs =>
            val tables = AvroTableOperations.tableData(configuration)

            val primaryTableSchema = schemas.get(exe._2.table).get
            val joinTableSchema = schemas.get(exe._2.joinTable).get
            val resultTableSchema = AvroTableOperations.joinSchema(primaryTableSchema, joinTableSchema)

            val avroFileWriter = new AvroFileWriter(resultTableSchema, exe._2.out, "join-result")

            val joinTableData = tables.get(exe._2.joinTable).get
            val primaryTableData = tables.get(exe._2.table).get

            val ids = new AtomicLong()

            //
            // insert join table data
            //
            joinTableData.foreach {
              ri =>
                val id = ids.incrementAndGet()
                rdbs.joinTableDb.put(Longs.toByteArray(id), AvroTableOperations.convertAvroRecordToBinary(ri))
                val joinPathValue = AvroTableOperations.getValueAtPath(exe._2.joinTableField, ri)

                val idBytes = Longs.toByteArray(id)
                val joinValueEncoded = DigestUtils.md5(AvroTableOperations.toBytes(joinPathValue))

                rdbs.joinTableDb.put(idBytes, AvroTableOperations.convertAvroRecordToBinary(ri))

                System.arraycopy(joinValueEncoded, 0, joinTableId, 0, 16)
                System.arraycopy(idBytes, 0, joinTableId, 16, 8)

                rdbs.joinTableFieldDb.put(joinTableId, idBytes)
            }

            val readOptions = new ReadOptions().setTotalOrderSeek(true)
            val joinsIt = rdbs.joinTableFieldDb.newIterator(readOptions)
            val tempId = Array.ofDim[Byte](16)
            //
            // making join by iterating primary table
            //
            primaryTableData.foreach {
              d =>
                val joinPathValue = AvroTableOperations.getValueAtPath(exe._2.joinTableField, d)
                val joinValueEncoded = DigestUtils.md5(AvroTableOperations.toBytes(joinPathValue))

                joinsIt.seek(joinValueEncoded)
                joinsIt.seekToFirst()

                new Iterator[(Array[Byte], Array[Byte])]() {
                  override def hasNext: Boolean = true

                  override def next(): (Array[Byte], Array[Byte]) = {
                    joinsIt.next()
                    (joinsIt.key(), joinsIt.value())
                  }
                }.takeWhile {
                  v =>
                    System.arraycopy(v._1, 0, tempId, 0, 16)
                    util.Arrays.equals(joinValueEncoded, tempId)
                }.foreach {
                  v =>
                    val avro = AvroTableOperations.convertBinaryToAvroRecord(rdbs.joinTableDb.get(v._2), joinTableSchema)
                    //TODO
                    // - check exact equal (md5 collisions)
                    val joinedAvro = AvroTableOperations.joinRecords(resultTableSchema, d, avro)
                    avroFileWriter.writeDoc(joinedAvro)
                }


            }

            avroFileWriter.close()

        }

    }

  }


}
